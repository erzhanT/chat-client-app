import axiosApi from "../axiosApi";
import {NotificationManager} from 'react-notifications';


export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const ON_CHANGE = 'ON_CHANGE';
export const GET_LAST_DATE = 'GET_LAST_DATE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const createMessagesSuccess = data => ({type: CREATE_MESSAGE_SUCCESS, data});
export const onChange = (e) => ({type: ON_CHANGE, e});


export const fetchMessages = () => {
    return async dispatch => {
        try {
            dispatch(fetchMessagesRequest());
            const response = await axiosApi.get('/messages');
            dispatch(fetchMessagesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMessagesFailure());

        }
    }
}

export const createMessage = (data) => {
    return async dispatch => {
        try {
            const obj = {
                author: data.author,
                message: data.message
            }
            const response = await axiosApi.post('/messages', obj);
            dispatch(createMessagesSuccess(response.data));
        } catch (e) {
            NotificationManager.error('Fill in the form fields!');
        }
    }
}

