import {
    CREATE_MESSAGE_SUCCESS,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS, GET_LAST_DATE,
    ON_CHANGE
} from "./actions";


const initialState = {
    messages: [],
    author: '',
    message: '',
    lastDate: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST :
            return {...state};
        case FETCH_MESSAGES_SUCCESS :
            return {...state, messages: action.messages};
        case FETCH_MESSAGES_FAILURE:
            return {...state}
        case ON_CHANGE:
            return {...state, [action.e.name]: action.e.value};
        case CREATE_MESSAGE_SUCCESS:
            return {...state, data: action.data};
        case GET_LAST_DATE:
            return {...state, date: action.date};
        default:
            return state
    }
};

export default reducer;