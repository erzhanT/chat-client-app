import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import reducer from "./store/reducer";
import thunk from "redux-thunk";
import {NotificationContainer} from "react-notifications";

import "react-notifications/lib/notifications.css";

const store = createStore(reducer, applyMiddleware(thunk))

const app = (
    <Provider store={store}>
        <NotificationContainer/>
        <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

