import React from 'react';
import {Card, CardContent, CardHeader, Grid, Typography} from "@material-ui/core";

const MessageItem = ({author, message, date, id}) => {
    return (
        <Grid item xs id={id}>
            <Card>
                <CardHeader title={date}/>
                <CardContent>

                    <Typography content={'p'}>
                        <strong>
                            Author: {author}
                        </strong>
                    </Typography>
                </CardContent>
                <CardContent>
                    <Typography content={'p'}>
                       Message: {message}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default MessageItem;