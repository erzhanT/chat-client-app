import './App.css';
import {Container, CssBaseline} from "@material-ui/core";
import MessagesContainer from "./containers/MessagesContainer/MessagesContainer";
import PostMessage from "./containers/PostMessage/PostMessage";

const App = () => {


    return (
        <>
            <CssBaseline/>
            <Container>
                <PostMessage/>
                <MessagesContainer/>
            </Container>
        </>
    )
};

export default App;
