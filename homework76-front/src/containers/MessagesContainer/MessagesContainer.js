import React, {useEffect, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages, getLast, getLastDate} from "../../store/actions";
import {Grid} from "@material-ui/core";
import MessageItem from "../../components/MessageItem/MessageItem";
import axiosApi from "../../axiosApi";

const MessagesContainer = () => {

    const dispatch = useDispatch();
    const state = useSelector(state => state);


    useEffect(() => {
        setInterval(() => {
            dispatch(fetchMessages());
        }, 2000)
    }, [dispatch]);





    return (
        <Grid container direction="column" spacing={2}>
            {state.messages.map(message => {
                return (
                    <MessageItem
                        key={message.id}
                        author={message.author}
                        message={message.message}
                        date={message.date}
                    />
                )
            })}
        </Grid>

    );
};

export default MessagesContainer;