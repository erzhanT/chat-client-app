import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createMessage, onChange} from "../../store/actions";
import {Button, Grid, TextField} from "@material-ui/core";

const PostMessage = () => {

    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const submitFormHandler = (e) => {
        e.preventDefault();
        dispatch(createMessage(state));
    }

    const onChangeHandler = (e) => {
        dispatch(onChange(e.target));
    };

    return (
        <form onSubmit={submitFormHandler}>
            <Grid container direction={"column"} spacing={2}>
                <Grid item>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id='author'
                        label='Author'
                        name='author'
                        value={state.author}
                        onChange={onChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id='message'
                        label='Message'
                        name='message'
                        value={state.message}
                        onChange={onChangeHandler}
                    />
                </Grid>
            </Grid>
            <Button type={"submit"} variant="contained" color="primary">Send message</Button>
        </form>
    );
};

export default PostMessage;