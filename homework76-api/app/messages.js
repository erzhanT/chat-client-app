const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();

router.get('/', (req, res) => {
    const messages = fileDb.getMessages();
    res.send(messages);
});

router.get('/:date', (req, res) => {
    res.send(fileDb.getNewMessagesByDate(req.params.date));
});

router.post('/', (req, res) => {

    if (req.body.message === '' || req.body.author === '') {
        res.status(400).send('Fill in the form fields!')
    } else {
        fileDb.addMessage(req.body);
        res.send(req.body);
    }


});

module.exports = router;