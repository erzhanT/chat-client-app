const express = require('express');
const cors = require("cors");
const fileDb = require('./fileDb');
const messages = require('./app/messages');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());

fileDb.init();


app.use('/messages', messages);

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});

