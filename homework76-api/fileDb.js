const fs = require('fs');
const {nanoid} = require("nanoid");

const filename = './db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getMessages() {
        return data.slice(-30);
    },
    addMessage(message) {
        message.id = nanoid();
        message.date = new Date().toISOString();
        data.push(message);
        this.save();
    },
    getNewMessagesByDate(date) {
        const messages = [];
        for (let item of data) {
            if(item.date > date) {
                messages.push(item);
            }
        }
        if(messages.length > 30) {
            return messages.slice(-30)
        }
        return messages;
    },
    save() {
        fs.writeFileSync(filename, JSON.stringify(data, null, 2));
    }
};